const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {

		
		if(!req.body.hasOwnProperty("name") || req.body.name === ""){
			return res.status(400).send({
				error: 'Bad Request - missing required parameter NAME or NAME is an empty string'
			})
		}

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				error: 'Bad Request - NAME has to be a string'
			})
		}

		if(!req.body.hasOwnProperty("ex")){
			return res.status(400).send({
				error: 'Bad Request - missing required parameter EX'
			})
		}

		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				error: 'Bad Request - EX has to be an object'
			})
		}

		if(Object.keys(req.body.ex).length == 0){
			return res.status(400).send({
				error: 'Bad Request - EX is an empty object'
			})
		}

		if(!req.body.hasOwnProperty("alias") || req.body.alias === ""){
			return res.status(400).send({
				error: 'Bad Request - missing required parameter ALIAS or ALIAS is an empty string'
			})
		}

		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				error: 'Bad Request - ALIAS has to be a string'
			})
		}



		let foundAlias = exchangeRates.find((entry) => {

			return entry.alias === req.body.alias

		});

		if(foundAlias){
			return res.status(400).send({
				error: 'Bad Request - duplicate entry found'
			})
		}

		if(!foundAlias){
			return res.status(200).send({
				success: 'There are no duplicate entries'
			})
		}


	})

module.exports = router;
