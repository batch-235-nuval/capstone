const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	

	it('test_api_post_currency_returns_200_if_complete_input_given', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			
		})
	})

	it('[Test Case 01] : Check if post/currency is running', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})

	it('[Test Case 02] : Check if post/currency returns status 400 if name is missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "nvl",
			//name: "Nuval",
			ex: {
				peso: 100,
				usd: 50,
				won: 200,
				yuan: 500,
				yen: 300
				}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 03] : Check if post/currency returns status 400 if name is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "nvl",
			name: 1000,
			ex: {
				peso: 100,
				usd: 50,
				won: 200,
				yuan: 500,
				yen: 300
				}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 04] : Check if post/currency returns status 400 if name is an empty string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "nvl",
			name: "",
			ex: {
				peso: 100,
				usd: 50,
				won: 200,
				yuan: 500,
				yen: 300
				}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 05] : Check if post/currency returns status 400 if ex is missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "nvl",
			name: "Nuval"
			//ex: {
			//	peso: 100,
			//	usd: 50,
			//	won: 200,
			//	yuan: 500,
			//	yen: 300
			//	}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 06] : Check if post/currency returns status 400 if ex is not an object', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "nvl",
			name: "Nuval",
			ex: "{peso: 100, usd: 50, won: 200, yuan: 500, yen: 300}"
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 07] : Check if post/currency returns status 400 if ex is an empty object', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "nvl",
			name: "Nuval",
			ex: {}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 08] : Check if post/currency returns status 400 if alias is missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			//alias: 1000,
			name: "Nuval",
			ex: {
				peso: 100,
				usd: 50,
				won: 200,
				yuan: 500,
				yen: 300
				}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 09] : Check if post/currency returns status 400 if alias is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: 1000,
			name: "Nuval",
			ex: {
				peso: 100,
				usd: 50,
				won: 200,
				yuan: 500,
				yen: 300
				}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 10] : Check if post/currency returns status 400 if alias is an empty string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "",
			name: "Nuval",
			ex: {
				peso: 100,
				usd: 50,
				won: 200,
				yuan: 500,
				yen: 300
				}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 11] : Check if post/currency returns status 400 if all fields are complete but there is a duplicate alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "usd",
			name: "Nuval",
			ex: {
				peso: 100,
				usd: 50,
				won: 200,
				yuan: 500,
				yen: 300
				}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('[Test Case 12] : Check if post/currency returns status 200 if all fields are complete and there are no duplicates', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type("json")
		.send({
			alias: "usd",
			name: "Nuval",
			ex: {
				peso: 100,
				usd: 50,
				won: 200,
				yuan: 500,
				yen: 300
				}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

})

